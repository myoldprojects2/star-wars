local composer = require("composer")
local scene = composer.newScene()

--здесь размещать глобальные переменные и пользовательские функции
local Notes
local Text1
local Text2
local Text3
local closeNote
local widget

--переход в меню
function menu(event)
    Start_click = audio.play( Click, {loops = 0} )
    composer.gotoScene("menu", {effect = "fade"})
end

function scene:create(event)--графика и аудио
    local sceneGroup = self.view

    widget = require("widget")

    Click = audio.loadSound( "клик.mp3" )

    Notes = display.newRect(display.contentCenterX, display.contentCenterY, 570, 350)
    Notes:setFillColor(0, 0, 0, 0.2)

    Text1 = display.newText( "Добро пожаловать в игру", display.contentCenterX, 80, "Helvetica", 30 )
    Text2 = display.newText( "Star Wars: по планетам !", display.contentCenterX, 125, "fontSW", 30 )
    Text2:setFillColor(1, 0.8, 0.2)
    Text3 = display.newText( "Здесь вас ждут путешествия по трем\n   различным планетам, на которых\n  вам предстоит выполнять задания\n             различной сложности!", display.contentCenterX, 220, "Helvetica", 30 )

    --кнопка закрыть
    closeNote = widget.newButton {
        width = 30, height = 30, -- размеры кнопки
        left = 410, top = 20, -- положение кнопки
        fontSize = 18, -- размер шрифта
        labelColor = { default = { 1, 0.8, 0.2 }, over = { 1, 0.9, 0.7 } }, -- цвет текста
        label = "закрыть", -- текст на кнопке
        font = "fontSW",

        onEvent = menu
    }

    sceneGroup:insert(Notes)
    sceneGroup:insert(Text1)
    sceneGroup:insert(Text2)
    sceneGroup:insert(Text3)
    sceneGroup:insert(closeNote)
end

function scene:show(event)--визуализация показа сцены
    local phase = event.phase

    if (phase == "did") then
        composer.removeScene("menu")--удалить предыдущую сцену
    end
end

function scene:hide(event)--вызывается когда закрывается сцена
    local phase = event.phase

    if (phase == "will") then--тут можно отключить флновую музыку и тд
        Notes = nil
        Text1 = nil
        Text2 = nil
        Text3 = nil
        closeNote = nil
        widget = nil
    end
    if (phase == "did") then
        composer.removeScene("Spravka")
    end
end

function scene:destroy(event)--для освобождения памяти от сцены
    -- body
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)

return scene