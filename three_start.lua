local composer = require("composer")
local scene = composer.newScene()

--здесь размещать глобальные переменные и пользовательские функции
local alarmMusic
local Start_alarm
local timeLimit
local E
local timer1

function to_three(event)
    display.remove(E)

    composer.gotoScene("three_level", {effect = "fade"})
end

local function timerDown()
    timeLimit = timeLimit - 1

    if ( timeLimit == 4 ) then
        E = display.newImage("files_E/E.png", display.contentCenterX, display.contentCenterY + 10)
        E:scale( 1.7, 1.7 )

        --фоновая музыка
        local backMusic = audio.loadStream( "files_E/three_music.mp3", { channel = 1 } )
        backMusicChannel = audio.play( backMusic )
        audio.setVolume( 0.4, { channel = 1, fadein = 10000 } )
    end

    if (timeLimit == 0) then
        to_three()
    end
end

function scene:create(event)--графика и аудио
    local sceneGroup = self.view

    alarmMusic = audio.loadStream( "files_H/alarm2.mp3" )

    timeLimit = 5
end

function scene:show(event)--визуализация показа сцены
    local phase = event.phase

    if (phase == "did") then
        composer.removeScene("Levels")--удалить предыдущую сцену
        
        timer1 = timer.performWithDelay(1000, timerDown, timeLimit)
    end
end

function scene:hide(event)--вызывается когда закрывается сцена
    local phase = event.phase

    if (phase == "will") then--тут можно отключить флновую музыку и тд
        alarmMusic = nil
        Start_alarm = nil
        timeLimit = nil
        E = nil
        timer1 = nil
    end
    if (phase == "did") then
        composer.removeScene("three_start")
    end
end

function scene:destroy(event)--для освобождения памяти от сцены
    -- body
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)

return scene