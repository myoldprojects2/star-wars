local composer = require("composer")
local scene = composer.newScene()

--здесь размещать глобальные переменные и пользовательские функции
local Start_click
local Click
local widget
local physics
local bul
local bulE
local bulE1
local bulE2
local bomb
local vert
local forTouch
local back
local Gran1
local finish
local delete
local Base
local Gun1
local Gun2
local ATAT
local Text1
local Text2
local Score
local Lives
local s
local l
local Enemy
local f
local ScoreP
local timeSh
local timer1
local timer2
local timer3
local timer4
local timer5
local timer6
local timerSh
local note
local testH
local Text3
local Menu
local relonch
local Right
local Shield
local Boom

--функция перехода в меню
local function Levels(event)
    Start_click = audio.play( Click, {loops = 0} )
    audio.pause( backMusicChannel )
    composer.gotoScene("menu", {effect = "fade"})
end

--функция перезапуска уровня
local function Restart(event)
    Start_click = audio.play( Click, {loops = 0} )
    audio.pause( backMusicChannel )
    composer.gotoScene("two_start", {effect = "fade"})
end

--фоновая музыка
local backMusic = audio.loadStream( "files_H/two_music.mp3", { channel = 1 } )
local backMusicChannel = audio.play( backMusic )
audio.setVolume( 0.2, { channel = 1, fadein = 10000 } )

--бластер
local function shot()
    local shotMusic = audio.loadSound( "files_H/blaster.mp3" )
    audio.play( shotMusic )
end 

--бомба
local function bigBomb()
    local bombMusic = audio.loadSound( "files_H/bomb.mp3" )
    audio.play( bombMusic )
    audio.setVolume( 0.8 )
end

--пушка
local function shotGun()
    local gunMusic = audio.loadSound( "files_H/shootGun.mp3" )
    audio.play( gunMusic )
    audio.setVolume( 0.5 )
end

function scene:create(event)--графика и аудио
    local sceneGroup = self.view

    Click = audio.loadSound( "клик.mp3" )

    widget = require("widget")
    physics = require "physics"
    physics.start()
    
    vert = {120,-270, 460,-270, 460,50, 120,50}
    forTouch = display.newPolygon(350, display.contentCenterY, vert)
    forTouch:setFillColor(0, 0, 0, 0.1)

    --фон
    back = display.newRect(955, 160, 2000, 320)
    back.fill = { type = "image", filename = "files_H/Hoth.png" }

    --границы
    Gran1 = display.newRect( 245, 300, 590, 5 )
    Gran1:setFillColor(1, 0, 0, 0.0)
    physics.addBody( Gran1, "static" )

    finish = display.newRect( 1595, 160, 5, 350)
    finish:setFillColor(0, 1, 0, 0.0)
    finish.ID = "finish"
    physics.addBody( finish, "dinamic")
    finish.isSensor = true
    finish.gravityScale = 0

    delete = display.newRect( -200, 160, 5, 350)
    delete:setFillColor(0, 1, 0, 0.0)
    delete.ID = "delete"
    physics.addBody( delete, "dinamic")
    delete.isSensor = true
    delete.gravityScale = 0

    --финал
    Base = display.newRect( 1825, 260, 200, 110 )
    Base.fill = { type = "image", filename = "files_H/Gener.png" }
    physics.addBody( Base, "dinamic")
    Base.isSensor = true
    Base.gravityScale = 0

    Gun1 = display.newRect( 1690, 245, 65, 65 )
    Gun1.fill = { type = "image", filename = "files_H/gun.png" }
    physics.addBody( Gun1, "dinamic")
    Gun1.isSensor = true
    Gun1.ID = "gun"
    Gun1.gravityScale = 0

    Gun2 = display.newRect( 1810, 280, 65, 65 )
    Gun2.fill = { type = "image", filename = "files_H/gun.png" }
    physics.addBody( Gun2, "dinamic")
    Gun2.isSensor = true
    Gun2.ID = "gun"
    Gun2.gravityScale = 0

    --объект
    ATAT = display.newRect( 140, 200, 165, 135)
    ATAT.fill = { type = "image", filename = "files_H/AT_AT1.png" }
    physics.addBody(ATAT, "dinamic", {bounce = 0.5})
    ATAT.gravityScale = 0.8
    ATAT.ID = "obj"

    --счет
    Text1 = display.newText(  "Счет:", 10, 20, "fontSW", 20 )
    Text1:setFillColor( 0.1, 0.2, 0.5 )
    Text1:toFront()
    s = 0
    Score = display.newText( s, 65, 20, "fontSW", 20 )
    Score:setFillColor( 0.1, 0.2, 0.5 )
    Score:toFront()

    --жизни
    Text2 = display.newText( "Жизни:", 410, 20, "fontSW", 20 )
    Text2:setFillColor( 0.6, 0.1, 0.1 )
    Text2:toFront()
    l = 100
    Lives = display.newText( l, 490, 20, "fontSW", 20 )
    Lives:setFillColor( 0.6, 0.1, 0.1 )
    Lives:toFront()

    --истребители
    function Enemies()
        local x = math.random(600, 800)
        local y = math.random(100, 300)
        Enemy = display.newRect( x, y, 110, 50)
        sceneGroup:insert(Enemy)
        physics.addBody( Enemy, "dinamic")
        Enemy.isSensor = true
        Enemy.gravityScale = 0
        Enemy.fill = { type = "image", filename = "files_H/Enemy.png" }
        Enemy.ID = "enemy"
        Enemy:applyLinearImpulse( -0.2, 0, Enemy.x, Enemy.y)
        timer3 = timer.performWithDelay( 1500, ShootEnemy, 1 )

        local function crash(self, event)
            if (event.phase == "began") then
                if (event.other.ID == "bul") then
                    s = s + 2
                    Score.text = s

                    self:removeSelf()
                    event.other:removeSelf()
                end

                if (event.other.ID == "obj") then
                    l = l - 2
                    Lives.text = l
                end

                if (event.other.ID == "shit") then
                    self:removeSelf()
                end

                if (event.other.ID == "delete") then
                    self:removeSelf()
                end
            end
        end
        Enemy.collision = crash
        Enemy:addEventListener("collision", Enemy)
    end

    --стреляет ATAT
    function Shoot(event)
        if (event.phase == "began") then
            bul = display.newRect( ATAT.x, ATAT.y, 5, 30 )
            sceneGroup:insert(bul)
            bul.rotation = math.ceil(math.atan2((event.y - bul.y), (event.x - bul.x)) * 180 / math.pi) + 90
            bul:setFillColor( 0.8, 0.1, 0.1 )
            physics.addBody( bul, "dinamic", {isSensor = true} )
            bul.gravityScale = 0
            bul.ID = "bul"
            local ang = -math.rad(bul.rotation + 90)
            local vX = math.cos(ang)
            local vY = - math.sin(ang)
            bul:applyLinearImpulse( -0.07 * vX, -0.07 * vY, ATAT.x, ATAT.y)
            shot()
        end
    end

    --стреляют пушки
    function ShootEnemy1()
        bulE = display.newRect(Gun1.x, Gun1.y, 23, 4 )
        sceneGroup:insert(bulE)
        bulE:setFillColor( 0, 1, 0 )
        physics.addBody( bulE, "dinamic", {isSensor = true} )
        bulE.gravityScale = 0
        bulE.ID = "bulE"
        bulE:applyLinearImpulse( -0.1, 0, Gun1.x, Gun1.y)
        shotGun()
    end

    function ShootEnemy2()
        bulE1 = display.newRect( Gun2.x, Gun2.y, 23, 4 )
        sceneGroup:insert(bulE1)
        bulE1:setFillColor( 0, 1, 0 )
        physics.addBody( bulE1, "dinamic", {isSensor = true} )
        bulE1.gravityScale = 0
        bulE1.ID = "bulE"
        bulE1:applyLinearImpulse( -0.1, 0, Gun2.x, Gun2.y)
        shotGun()
    end

    --стреляет ATAT бомбами
    function Bomb(event)
        if (event.phase == "began") then
            bomb = display.newRect( ATAT.x, ATAT.y, 100, 50 )
            sceneGroup:insert(bomb)
            bomb.fill = { type = "image", filename = "files_H/bomb.png" }
            physics.addBody( bomb, "dinamic", {isSensor = true} )
            bomb.gravityScale = 0
            bomb.ID = "bomb"
            bomb:applyLinearImpulse( 0.5, 0, ATAT.x, ATAT.y)
            bigBomb()
        end
    end

    --стреляют истребители
    function ShootEnemy()
        if (Enemy.x ~= nil) then
            bulE2 = display.newRect( Enemy.x, Enemy.y, 23, 4 )
            sceneGroup:insert(bulE2)
            bulE2:setFillColor( 0, 1, 1 )
            physics.addBody( bulE2, "dinamic", {isSensor = true} )
            bulE2.gravityScale = 0
            bulE2.ID = "bulE"
            bulE2:applyLinearImpulse( -0.1, 0, Enemy.x, Enemy.y)
            shotGun()
        end
    end

    --двигать карту и объекты
    function moveAll()
        local targetX = back.x - 30
        transition.moveTo(back, {x = targetX, y, time = 200})

        local targetF = finish.x - 30
        transition.moveTo(finish, {x = targetF, y, time = 200})

        --препятствия
        local target1 = Base.x - 30
        transition.moveTo(Base, {x = target1, y, time = 200})

        local target2 = Gun1.x - 30
        transition.moveTo(Gun1, {x = target2, y, time = 200})

        local target3 = Gun2.x - 30
        transition.moveTo(Gun2, {x = target3, y, time = 200})

        local function AT_AT1()
            ATAT.fill = { type = "image", filename = "files_H/AT_AT2.png" }
        end
        timer1 = timer.performWithDelay( 200, AT_AT1, 1 )

        local function AT_AT2()
            ATAT.fill = { type = "image", filename = "files_H/AT_AT1.png" }
        end
        timer2 = timer.performWithDelay( 600, AT_AT2, 1 )
    end

    function onShield()--щит
        local Shield = display.newRect( 170, 220, 140, 185)
        sceneGroup:insert(Shield)
        Shield.fill = { type = "image", filename = "files_H/shield.png" }
        physics.addBody(Shield, "dinamic")
        Shield.gravityScale = 0
        Shield.isSensor = true

        local function stopSh()
            Shield:removeEventListener("collision", Shield)
            display.remove(Shield)
            Shield = nil
        end
        timerSh = timer.performWithDelay(5000, stopSh, 1)

        local function toShield(self, event)
            if (event.phase == "began") then
                if ((event.other.ID == "bulE") or (event.other.ID == "enemy")) then
                    event.other:removeSelf()
                end
            end
        end
        Shield.collision = toShield
        Shield:addEventListener("collision", Shield)
    end

    --кнопки
    --вправо
    Right = widget.newButton    
    {
        defaultFile = "buttons/right.png",
        overFile = "buttons/rightOver.png",
        width = 85, height = 85,
        x = 0,
        y = 277,  

        onEvent = moveAll
    }

    --щит
    Shield = widget.newButton    
    {
        defaultFile = "buttons/shield1.png",
        overFile = "buttons/shield2.png",
        width = 60, height = 60,
        x = 0,
        y = 200,  

        onEvent = onShield
    }

    function FinRaund()
        f = 100
        ScoreP = display.newText( f, 435, 180, "fontSW", 20 )
        ScoreP:setFillColor( 0.5, 0, 0.5 )
        ScoreP:toFront()
        sceneGroup:insert(ScoreP)

        display.remove(Right)

        timer5 = timer.performWithDelay(1500, ShootEnemy1, 0)
        timer6 = timer.performWithDelay(2000, ShootEnemy2, 0)

        Boom = widget.newButton
        {
            defaultFile = "buttons/boom1.png",
            overFile = "buttons/boom2.png",
            width = 85, height = 85,
            x = 0,
            y = 277,

            onEvent = Bomb
        }
        sceneGroup:insert(Boom)

        local function BaseCrash(self, event)
            if (event.phase == "began") then
                if (event.other.ID == "bul") then
                    f = f - 2
                    ScoreP.text = f

                    s = s + 2
                    Score.text = s
                
                    event.other:removeSelf()
                end

                if (event.other.ID == "bomb") then
                    f = f - 10
                    ScoreP.text = f

                    s = s + 10
                    Score.text = s
    
                    event.other:removeSelf()
                end

                if ((f == 0) or (f < 0)) then
                    timer.cancel(timer3)
                    timer.cancel(timer4)
                    timer.cancel(timer5)
                    timer.cancel(timer6)
                    display.remove(ScoreP)
                    ScoreP = nil
                    display.remove(Right)
                    Right = nil
                    display.remove(Shield)
                    Shield = nil
                    display.remove(Boom)
                    Boom = nil
                    forTouch:removeEventListener("touch", Shoot)

                    Text1.x = display.contentCenterX - 20
                    Text1.y = 140
                    Text1.size = 27
                    Score.x = display.contentCenterX + 55
                    Score.y = 140
                    Score.size = 27

                    Text2.x = display.contentCenterX - 20
                    Text2.y = 190
                    Text2.size = 27
                    Lives.x = display.contentCenterX + 65
                    Lives.y = 190
                    Lives.size = 27
                    
                    Text3 = display.newText( "УРОВЕНЬ ПРОЙДЕН", display.contentCenterX, 80, "fontSW", 40 )
                    Text3:setFillColor( 0.1, 0.2, 0.5 )
                    Text3:toFront()
                    sceneGroup:insert(Text3)

                    --кнопка перехода к уровням
                    Menu = widget.newButton {
                        width = 140, height = 50, -- размеры кнопки
                        defaultFile = "buttons/toMenu.png",
                        overFile = "buttons/toMenu1.png",
                        x = display.contentCenterX,
                        y = 260,
                        onEvent = Levels
                    }
                    sceneGroup:insert(Menu)

                    Base.fill = { type = "image", filename = "files_H/GenerBrok.png" }
                    Gun1.fill = { type = "image", filename = "files_H/gunBrok.png" }
                    Gun2.fill = { type = "image", filename = "files_H/gunBrok.png" }  
                    
                    physics.pause()
                end
            end
        end
        Base.collision = BaseCrash
        Base:addEventListener("collision", Base)
    end

    --если кончились жизни
    function AT_ATBrok(event)
        timer.cancel(timer3)
        timer.cancel(timer4)
        timer.cancel(timer5)
        timer.cancel(timer6)
        display.remove(ScoreP)
        ScoreP = nil
        display.remove(Right)
        Right = nil
        display.remove(Shield)
        Shield = nil
        display.remove(Boom)
        Boom = nil
        forTouch:removeEventListener("touch", Shoot)

        Text1.x = display.contentCenterX - 20
        Text1.y = 140
        Text1.size = 27
        Score.x = display.contentCenterX + 55
        Score.y = 140
        Score.size = 27

        Text2.x = display.contentCenterX - 20
        Text2.y = 190
        Text2.size = 27
        Lives.x = display.contentCenterX + 65
        Lives.y = 190
        Lives.size = 27
        
        Text3 = display.newText( "УРОВЕНЬ ПРОВАЛЕН", display.contentCenterX, 80, "fontSW", 40 )
        Text3:setFillColor( 0.1, 0.2, 0.5 )
        Text3:toFront()
        sceneGroup:insert(Text3)

        --кнопка перехода к уровням
        Menu = widget.newButton {
            width = 140, height = 50, -- размеры кнопки
            defaultFile = "buttons/toMenu.png",
            overFile = "buttons/toMenu1.png",
            x = 120,
            y = 260,
            onEvent = Levels
        }
        sceneGroup:insert(Menu)

        --кнопка перехода к уровням
        relonch = widget.newButton {
            width = 200, height = 50, -- размеры кнопки
            defaultFile = "buttons/relonch1.png",
            overFile = "buttons/relonch2.png",
            x = 350,
            y = 260,
            onEvent = Restart
        }
        sceneGroup:insert(relonch)

        ATAT.width = 220
        ATAT.height = 100
        ATAT.fill = { type = "image", filename = "files_H/AT_ATBrok.png" }

        physics.pause()
    end

    --финиш
    function stop(self, event)
        if (event.phase == "began") then
            if (event.other.ID == "bulE") then
                l = l - 2
                Lives.text = l
            
                event.other:removeSelf()
            end

            if (event.other.ID == "finish") then
                FinRaund()
            end

            if (l < 1) then
                AT_ATBrok()
            end
        end
    end
    ATAT.collision = stop
    ATAT:addEventListener("collision", ATAT)

    sceneGroup:insert(forTouch)
    sceneGroup:insert(back)
    sceneGroup:insert(Gran1)
    sceneGroup:insert(finish)
    sceneGroup:insert(delete)
    sceneGroup:insert(Base)
    sceneGroup:insert(Gun1)
    sceneGroup:insert(Gun2)
    sceneGroup:insert(ATAT)
    sceneGroup:insert(Text1)
    sceneGroup:insert(Text2)
    sceneGroup:insert(Score)
    sceneGroup:insert(Lives)
    sceneGroup:insert(Right)
    sceneGroup:insert(Shield)
end

function scene:show(event)--визуализация показа сцены
    local phase = event.phase

    if (phase == "did") then
        composer.removeScene("two_start")--удалить предыдущую сцену

        --истребители
        timer4 = timer.performWithDelay( 2000, Enemies, 15 )

        forTouch:addEventListener("touch", Shoot)
    end
end

function scene:hide(event)--вызывается когда закрывается сцена
    local phase = event.phase

    if (phase == "will") then
        Base:removeEventListener("collision", Base)
        ATAT:removeEventListener("collision", ATAT)
        physics.stop()
        forTouch = nil
        Baze = nil
        Start_click = nil
        backMusic = nil
        backMusicChannel = nil
        Click = nil
        physics = nil
        widget = nil
        ATAT = nil
        Enemy = nil
        bul = nil
        bulE = nil
        bulE1 = nil
        bulE2 = nil
        bomb = nil
        vert = nil
        back = nil
        Gran1 = nil
        finish = nil
        delete = nil
        Gun1 = nil
        Gun2 = nil
        Text1 = nil
        Text2 = nil
        Text3 = nil
        Score = nil
        Lives = nil
        bul = nil
        bulE = nil
        bulE1 = nil
        bulE2 = nil
        bomb = nil
        timer1 = nil
        timer2 = nil
        timer3 = nil
        timer4 = nil
        timer5 = nil
        timer6 = nil
        timerSh = nil
        Menu = nil
        relonch = nil
    end
    if (phase == "did") then
        composer.removeScene("two_level")
    end
end

function scene:destroy(event)--для освобождения памяти от сцены
    -- body
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)

return scene