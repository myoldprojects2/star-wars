local composer = require("composer")
local scene = composer.newScene()

--здесь размещать глобальные переменные и пользовательские функции
local alarmMusic
local Start_alarm
local timeLimit
local N
local timer1

function to_one(event)
    display.remove(N)
    composer.gotoScene("one_level", {effect = "fade"})
end

local function timerDown()
    timeLimit = timeLimit - 1
    Start_alarm = audio.play( alarmMusic, { loops = 0 } )

    if ( timeLimit == 4 ) then
        N = display.newImage("files_N/tatoine.png", display.contentCenterX, display.contentCenterY + 10)
        N:scale( 1.8, 1.8 )
    end

    if (timeLimit == 0) then
        to_one()
    end
end

function scene:create(event)--графика и аудио
    local sceneGroup = self.view

    alarmMusic = audio.loadStream( "files_N/Nabo.mp3" )

    timeLimit = 5
end

function scene:show(event)--визуализация показа сцены
    local phase = event.phase

    if (phase == "did") then
        composer.removeScene("Levels")--удалить предыдущую сцену
        
        timer1 = timer.performWithDelay(1000, timerDown, timeLimit)
    end
end

function scene:hide(event)--вызывается когда закрывается сцена
    local phase = event.phase

    if (phase == "will") then--тут можно отключить флновую музыку и тд
        alarmMusic = nil
        Start_alarm = nil
        timeLimit = nil
        N = nil
        timer1 = nil
    end
    if (phase == "did") then
        composer.removeScene("one_start")
    end
end

function scene:destroy(event)--для освобождения памяти от сцены
    -- body
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)

return scene