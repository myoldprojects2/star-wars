local composer = require("composer")
local scene = composer.newScene()

--здесь размещать глобальные переменные и пользовательские функции
local Click
local Start_click
local widget
local Text
local one
local two
local three
local closeLevels

--переход в меню
function menu(event)
    Start_click = audio.play( Click, {loops = 0} )
    composer.gotoScene("menu", {effect = "flipFadeOutIn"})
end

--переход к 1 уровню
function one_start(event)
    Start_click = audio.play( Click, {loops = 0} )
    audio.pause(backgroundMusicChanel)
    display.remove(fon)
    fon = nil
    composer.gotoScene("one_start", {effect = "fade"})
end

--переход ко 2 уровню
function two_start(event)
    Start_click = audio.play( Click, {loops = 0} )
    audio.pause(backgroundMusicChanel)
    display.remove(fon)
    fon = nil
    composer.gotoScene("two_start", {effect = "fade"})
end

--переход к 3 уровню
function three_start(event)
    Start_click = audio.play( Click, {loops = 0} )
    audio.pause(backgroundMusicChanel)
    display.remove(fon)
    fon = nil
    composer.gotoScene("three_start", {effect = "fade"})
end

function scene:create(event)--графика и аудио
    local sceneGroup = self.view

    widget = require("widget")

    Click = audio.loadSound( "клик.mp3" )

    Text = display.newText( "выберите уровень", display.contentCenterX, 50, "fontSW", 30 )
    Text:setFillColor(1, 0.8, 0.2)

    --кнопка уровень 1
    one = widget.newButton {
        width = 80, height = 80, -- размеры кнопки
        defaultFile = "buttons/1.png",
        overFile = "buttons/1.png",
        x = 80,
        y = 160,
        onEvent = one_start
    }

    --кнопка уровень 2
    two = widget.newButton {
        width = 80, height = 80, -- размеры кнопки
        defaultFile = "buttons/Hlevel.png",
        overFile = "buttons/Hlevel1.png",
        x = display.contentCenterX,
        y = 160,
        onEvent = two_start
    }

    --кнопка уровень 3
    three = widget.newButton {
        width = 80, height = 80, -- размеры кнопки
        defaultFile = "buttons/Elevel.png",
        overFile = "buttons/Elevel1.png",
        x = 400,
        y = 160,
        onEvent = three_start
    }

    --кнопка вернуться в меню
    closeLevels = widget.newButton {
        width = 30, height = 30, -- размеры кнопки
        left = 400, top = 270, -- положение кнопки
        fontSize = 16, -- размер шрифта
        labelColor = { default = { 1, 0.8, 0.2 }, over = { 1, 0.9, 0.7 } }, -- цвет текста
        label = "вернуться\n  в меню", -- текст на кнопке
        font = "fontSW",

        onEvent = menu
    }

    sceneGroup:insert(Text)
    sceneGroup:insert(one)
    sceneGroup:insert(two)
    sceneGroup:insert(three)
    sceneGroup:insert(closeLevels)
end

function scene:show(event)--визуализация показа сцены
    local phase = event.phase

    if (phase == "did") then
        composer.removeScene("menu")--удалить предыдущую сцену
    end
end

function scene:hide(event)--вызывается когда закрывается сцена
    local phase = event.phase

    if (phase == "will") then--тут можно отключить флновую музыку и тд
        Click = nil
        Start_click = nil
        widget = nil
        Text = nil
        one = nil
        two = nil
        three = nil
        closeLevels = nil
    end
    if (phase == "did") then
        composer.removeScene("Levels")
    end
end

function scene:destroy(event)--для освобождения памяти от сцены
    -- body
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)

return scene