local composer = require("composer")
local scene = composer.newScene()

--здесь размещать глобальные переменные и пользовательские функции
local alarmMusic
local Start_alarm
local timeLimit
local H
local timer1

function to_two(event)
    display.remove(H)
    audio.pause( Start_alarm )
    composer.gotoScene("two_level", {effect = "fade"})
end

local function timerDown()
    timeLimit = timeLimit - 1
    Start_alarm = audio.play( alarmMusic, { loops = 0 } )

    if ( timeLimit == 4 ) then
        H = display.newImage("files_H/H.png", display.contentCenterX, display.contentCenterY + 10)
        H:scale( 1.7, 1.7 )
    end

    if (timeLimit == 0) then
        to_two()
    end
end

function scene:create(event)--графика и аудио
    local sceneGroup = self.view

    alarmMusic = audio.loadStream( "files_H/alarm2.mp3" )

    timeLimit = 5
end

function scene:show(event)--визуализация показа сцены
    local phase = event.phase

    if (phase == "did") then
        composer.removeScene("Levels")--удалить предыдущую сцену
        composer.removeScene("two_level")
        
        timer1 = timer.performWithDelay(1000, timerDown, timeLimit)
    end
end

function scene:hide(event)--вызывается когда закрывается сцена
    local phase = event.phase

    if (phase == "will") then--тут можно отключить флновую музыку и тд
        alarmMusic = nil
        Start_alarm = nil
        timeLimit = nil
        H = nil
        timer1 = nil
    end
    if (phase == "did") then
        composer.removeScene("two_start")
    end
end

function scene:destroy(event)--для освобождения памяти от сцены
    -- body
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)

return scene