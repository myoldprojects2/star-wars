local composer = require("composer")
local scene = composer.newScene()

local widget
local note
local levels
local Click
local Start_click

--фон
fon = display.newImage("fon.png", display.contentCenterX, -50)
fon:scale( 1.2, 1.2 )
fon:toBack()

--функция перехода к примечанию
function Spravka(event)
    Start_click = audio.play( Click, {loops = 0} )
    composer.recycleOnSceneChange = true
    composer.gotoScene("Spravka", {effect = "fade"})
end

--функция перехода к уровням
function Levels(event)
    Start_click = audio.play( Click, {loops = 0} )
    composer.recycleOnSceneChange = true
    composer.gotoScene("Levels", {effect = "flip"})
end

function scene:create(event)--графика и аудио
    local sceneGroup = self.view

    widget = require("widget")
    Click = audio.loadSound( "клик.mp3" )

    --кнопка сведений об игре
    note = widget.newButton {
        width = 120, height = 30, -- размеры кнопки
        defaultFile = "buttons/note.png",
        overFile = "buttons/note1.png",
        x = display.contentCenterX,
        y = 210,
        onEvent = Spravka
    }

    --кнопка перехода к уровням
    levels = widget.newButton {
        width = 260, height = 60, -- размеры кнопки
        defaultFile = "buttons/began.png",
        overFile = "buttons/began1.png",
        x = display.contentCenterX,
        y = 150,
        onEvent = Levels
    }

    sceneGroup:insert(note)
    sceneGroup:insert(levels)
end

function scene:show(event)--визуализация показа сцены
    local phase = event.phase

    if (phase == "did") then
        composer.removeScene("Spravka")
        composer.removeScene("Levels")
        composer.removeScene("one_level")
        composer.removeScene("two_level")
        composer.removeScene("three_level")
    end
end

function scene:hide(event)--вызывается когда закрывается сцена
    local phase = event.phase

    if (phase == "will") then--тут можно отключить флновую музыку и тд
        Click = nil
        Start_click = nil
        widget = nil
        note = nil
        levels = nil
    end
    if (phase == "did") then
        composer.removeScene("menu")
    end
end

function scene:destroy(event)--для освобождения памяти от сцены
    -- body
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)

return scene