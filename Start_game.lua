local composer = require("composer")
local scene = composer.newScene()

--здесь размещать глобальные переменные и пользовательские функции
local Text
local fon
local SW
local timeLimit = 15
local timer1
local timer2
local timer3

--фон
local fon = display.newImage("fon.png", display.contentCenterX, 310)
fon:scale( 1.2, 1.2 )
fon:toBack()

--давным давно
local function begin()
    Text.text = "Давным давно, в одной\nдалекой-далекой галактике..."
    Text:setFillColor(0.3, 0.5, 0.8)
end

local function moveFon ()
    local function move()
        local targetY = fon.y - 360
        transition.moveTo(fon, {x, y = targetY, time = 5000})
    end
    timer1 = timer.performWithDelay( 50, move, 1 )
end

--star wars
local function SW()
    SW = display.newImage("SW.png", display.contentCenterX, display.contentCenterY)

    SW.width = 1600
    SW.height = 755

    local function zoom()
        SW:scale(0.97, 0.97)
    end
    timer2 = timer.performWithDelay( 50, zoom, 1100 )
end

local function timerDown()
    timeLimit = timeLimit - 1

    if( timeLimit == 14 )then
        begin()
    end

    if( timeLimit == 10 )then
        Text.text = " "
        backgroundMusicChannel = audio.play( backgroundMusic, { channel = 1, loops = 0, fadein = 2500 } )
        SW()
    end

    if( timeLimit == 5 )then
        moveFon()
    end

    if ( timeLimit == 0 ) then
        timer.cancel(timer2)
        display.remove(SW)
        display.remove(fon)
        composer.gotoScene("menu", {effect = "flip"})
    end
end

function scene:create(event)--графика и аудио
    local sceneGroup = self.view

    backgroundMusic = audio.loadStream( "main-theme.mp3" )
    Text = display.newText( " ", display.contentCenterX, display.contentCenterY, "Helvetica", 30)

    sceneGroup:insert(Text)
end

function scene:show(event)--визуализация показа сцены
    local phase = event.phase

    if (phase == "did") then
        timer3 = timer.performWithDelay(1000, timerDown, timeLimit)
    end
end

function scene:hide(event)--вызывается когда закрывается сцена
    local phase = event.phase

    if (phase == "will") then--тут можно отключить флновую музыку и тд
        Text = nil
        SW = nil
        timeLimit = nil
        timer1 = nil
        timer2 = nil
        timer3 = nil
    end
    if (phase == "did") then
        composer.removeScene("Start_game")
    end
end

function scene:destroy(event)--для освобождения памяти от сцены
    -- body
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)

return scene